package com.pap.uua.dto;

import java.io.Serializable;

/**
 * 登录对象
 */
public class UserDTO implements Serializable {

    /**
     * 用户名（可以为手机号）
     */
    private String userName;

    /**
     * 密码、验证码
     */
    private String password;

    /**
     * 作用范围
     *
     * B B端用户
     * C C端用户
     */
    private String domain;

    /**
     * 认证方式
     * password 密码登录
     * verifycode  验证码登录
     */
    private String authenticationType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", domain='" + domain + '\'' +
                ", authenticationType='" + authenticationType + '\'' +
                '}';
    }
}
