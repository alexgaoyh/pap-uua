package com.pap.uua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.pap.rbacfeignapi"})
@ComponentScan(basePackages = {"com.pap.uua","com.pap.rbacfeignapi"})
@EnableDiscoveryClient
public class PapUuaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PapUuaApplication.class, args);
    }

}